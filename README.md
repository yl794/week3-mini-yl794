# Welcome to week3 mini porject

This is a project for CDK development with TypeScript.

I created an S3 bucket using CDK and AWS CodeWhisper

To enable CodeWhisper, we need iinstall the extension and login the AWS account:

![](3.png)

Then I install the AWS CDK and CLI using node.js and initialize a typescript project.
I modified the source code with CodeWhisper to help.

![](2.png)

After run `cdk bootstrap` and `cdk deploy`, I can deploy the new app to AWS S3 bucket.
![](1.png)


