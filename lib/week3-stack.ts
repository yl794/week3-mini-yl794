import { App, Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { Bucket, BucketEncryption } from 'aws-cdk-lib/aws-s3';

export class Week3Stack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    new Bucket(this, 'MyEncryptedVersionedBucket', {
      versioned: true,
      encryption: BucketEncryption.S3_MANAGED,
    });
  }
}

const app = new App();
new Week3Stack(app, 'Week3Stack');
